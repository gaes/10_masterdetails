import { Component, OnInit,Input } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { Hero } from '../classes/hero';
import { Power } from '../classes/power';

@Component({
  selector: 'app-powers',
  templateUrl: './powers.component.html',
  styleUrls: ['./powers.component.css']
  
})
export class PowersComponent  {
  
  @Input() hero: Hero;

  powers:Power[]

  constructor() { 
    this.powers=[
      {"idHero":1 , "powerName":"can fly"},
      {"idHero":1 , "powerName":"can fight"},
      {"idHero":1 , "powerName":"can save people"},
      {"idHero":2 , "powerName":"can walk on the building "}
    ]
  }

  showPowers():Power[]{
    var filterpower:Power[] = [];    
    for (var power in this.powers) {
      if (this.powers[power].idHero == this.hero.id){   
        filterpower.push(this.powers[power]);
      }
    }
    return filterpower;
  }
  

}
