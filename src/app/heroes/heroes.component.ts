import { Component, OnInit } from '@angular/core';
import { Hero } from '../classes/hero';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  
  hero:Hero;
  heroes:Hero[];
  selectedHero:Hero;

  constructor() {
    this.heroes= [
      { 
        "id":1 , "name" :"Supermanss"
      },
      { 
        "id":2 , "name" :"Spiderman"
      },
      { 
        "id":3 , "name" :"Batman"
      }
    ];
    
  }

  ngOnInit() {
  }

  onSelect(hero:Hero) {
    console.log(hero);
    this.selectedHero = hero;
  }


}
